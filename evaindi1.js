//contador de tiradas a cero
var cont = 0;
//variable global heroe(para su posicion)
var heroe;
//variable de la posicion del heroe
var posHero = 0;
//variable que gusrdara la posicion pulsada
var posi = 0;
//variable record
var record = 0;



//funcion de validacion de nombre y envio por ajax
function enviarNombre() {

    var formu = document.getElementById("formu");
    //resetear elementos
    document.getElementById("primero").innerHTML = "";
    document.getElementById("jugar").style.display = "none";
    var respuesta = "";
    var sema = true;
    //recoge value del campo texto
    var nom = document.getElementById("nombre").value;
    //RegExps
    var reg1 = /.{4,}/;
    var reg2 = /\d/;

    //muestra mensajes de error
    if (!reg1.test(nom)) {
        alert("El nombre debe tener al menos 4 letras");
        sema = false;
    }
    if (reg2.test(nom)) {
        alert("Los números no son permitidos");
        sema = false;
    }
    //para que no salten varios alerts usamos un semaforo
    if (sema) {
        //instanciamos XML HttpRequest
        var httpReq = new XMLHttpRequest();
        //abrir conexion
        httpReq.open("POST", "http://hispabyte.net/DWEC/entregable1.php", true);
        //indicamos cabeceras
        httpReq.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        //definimos comportamiento onreadystatechange
        httpReq.onreadystatechange = function() {
                if (httpReq.readyState == 1) {
                    document.getElementById('primero').innerHTML = "Cargando";
                }
                //si se ha completado
                if (httpReq.readyState == 4) {
                    //si es correcto el status
                    if (httpReq.status == 200) {
                        //obtenemos cadena respuesta
                        respuesta = httpReq.responseText;
                    }
                }
                //si la respuesta es OK 
                if (respuesta == "OK") {
                    document.getElementById("primero").innerHTML = "A luchar héroe: " + nom;
                    //hace visible elemento
                    document.getElementById("jugar").style.display = "inline";
                } else if (respuesta == "ERROR") {
                    alert("El número de letras debe ser impar");
                }
            }
            //enviamos accion
        httpReq.send("nombre=" + nom);
    }
}

function jugar() {

    //quitamos mensaje
    document.getElementById("primero").innerHTML = "";

    //contador A cero
    document.getElementById("contador").value = 0;
    cont = 0;

    //resetea imagen del dado
    document.getElementById("dado").src = "../images/dado1.jpeg";
    //llama a la creacion de la tabla con las celdas
    crearTabla();

    //posiciona al heroe
    posHero = 0;
    heroe = document.getElementsByTagName("td")[posHero];
    rellenaHeroe();


    //hace desaparecer y aparecer elementos
    document.getElementById("pri").style.display = "none";
    document.getElementById("sec").style.display = "inline";
    document.getElementById("jugar").style.display = "none";


}

//rellena la casilla con el heroe
function rellenaHeroe() {
    heroe.innerHTML = "<img src='../images/heroe1.png' style='width:50px;'/>";
    heroe.style.border = '5px solid black';
}

//funcion que crea las filas y celdas con la imagen , borde y ancho
function crearTabla() {
    //resetea
    document.getElementById("segundo").innerHTML = "";
    var tblbody = document.createElement("tbody");

    var co = 0;
    for (var i = 1; i <= 15; i++) {
        var fila = document.createElement("tr");

        for (var j = 1; j <= 15; j++) {
            var celda = document.createElement("td");
            celda.id = co;
            var suelo = document.createElement("img");
            suelo.src = "../images/images.jpeg";
            suelo.setAttribute("class", "suelo");
            suelo.id = co;
            co++;
            suelo.style.width = "50px";
            celda.appendChild(suelo);
            celda.style.border = '5px solid black';


            fila.appendChild(celda);
        }
        tblbody.appendChild(fila);
    }

    document.getElementById("segundo").appendChild(tblbody);

    //quita listener
    for (var k = 0; k < 225; k++) {
        document.getElementsByTagName("td")[k].removeEventListener("click", moverlo);
    }

    //posiciona el cofre
    var cofre = document.getElementsByTagName("td")[224];
    cofre.innerHTML = "<img src='../images/cofre.jpg' style='width:50px;'/>";
    cofre.style.border = '5px solid black';
}

//incrementa el contador de tiradas
function contador() {
    cont++;
    document.getElementById("contador").value = cont;

}

//cuando la pagina se recarga resetea el input del contador
window.onload = function() {
    document.getElementById("contador").value = 0;
}

//funcion de tirada de dado
function tiradaAzar() {
    var numero = Math.random() * 6;
    var num = Math.ceil(numero);
    return num;
}

function asociaDado(num) {
    var dado = document.getElementById("dado");

    switch (num) {
        case 1:
            dado.src = "../images/dado1.jpeg";
            return 1;
        case 2:
            dado.src = "../images/dado2.png";
            return 2;
        case 3:
            dado.src = "../images/dado3.jpeg";
            return 3;
        case 4:
            dado.src = "../images/dado4.jpeg";
            return 4;
        case 5:
            dado.src = "../images/dado5.jpeg";
            return 5;
        case 6:
            dado.src = "../images/dado6.jpeg";
            return 6;
    }
}

//colorea las casillas donde se puede desplazar
function coloreaCasillas(num) {
    coloreaHztalI(num);
    coloreaHztalD(num);
    coloreaVertAb(num);
    coloreaVertArr(num);
}

//horizontal izquierda
function coloreaHztalI(num) {

    for (var i = 0; i <= num; i++) {
        if ((((posHero - i) % 15) <= (posHero % 15)) && ((posHero - i) >= 0)) {

            document.getElementsByTagName("td")[posHero - i].style.border = '5px solid red';
            document.getElementsByTagName("td")[posHero - i].addEventListener("click", moverlo);
            document.getElementById("tirar").style.display = "none";

        } else {
            break;
        }

    }

}

//horizontal derecha
function coloreaHztalD(num) {


    for (var i = 0; i <= num; i++) {

        if (((posHero + i) % 15) >= (posHero % 15)) {

            document.getElementsByTagName("td")[posHero + i].style.border = '5px solid red';
            document.getElementsByTagName("td")[posHero + i].addEventListener("click", moverlo);
            document.getElementById("tirar").style.display = "none";

        }

    }


}

//vertical abajo
function coloreaVertAb(num) {


    for (var i = 0; i <= (15 * num); i += 15) {

        if ((((posHero + i) % 15) >= (posHero % 15)) && ((posHero + i) < 225)) {

            document.getElementsByTagName("td")[posHero + i].style.border = '5px solid red';
            document.getElementsByTagName("td")[posHero + i].addEventListener("click", moverlo);
            document.getElementById("tirar").style.display = "none";

        } else {
            break;
        }

    }

}

//vertical arriba
function coloreaVertArr(num) {


    for (var i = (15 * num); i >= 0; i -= 15) {

        if ((((posHero - i) % 15) <= (posHero % 15)) && ((posHero - i) >= 0)) {

            document.getElementsByTagName("td")[posHero - i].style.border = '5px solid red';
            document.getElementsByTagName("td")[posHero - i].addEventListener("click", moverlo);
            document.getElementById("tirar").style.display = "none";

        } else {
            continue;
        }

    }

}

//funcion de mover el heroe
function moverlo() {

    posi = document.getElementById(this.id).id;
    crearTabla();
    posHero = parseInt(posi);
    heroe = document.getElementsByTagName("td")[posHero];
    rellenaHeroe();
    document.getElementById("tirar").style.display = "inline";
    llegada();

}

//chequea llegada a cofre
function llegada() {
    if (posHero == 224) {
        record = document.getElementById("contador").value;
        alert("Has llegado al cofre en " + record + " tiradas");
        chequeaRecord();
        document.getElementById("jugar").style.display = "inline";
        document.getElementById("sec").style.display = "none";
    }
}

//chequea record en localStorage
function chequeaRecord() {
    if (!localStorage.getItem("record")) {
        alert("Héroe, has establecido un record de tiradas de " + record + " tiradas.");
        localStorage.setItem("record", record);
    } else {
        var rAlm = parseInt(localStorage.getItem("record"));
        if (record > rAlm) {
            alert("No has superado el record actual de " + rAlm + " tiradas.")
        } else if (record == rAlm) {
            alert("Has igualado el record de tiradas establecido en " + record + " tiradas.")
        } else {
            alert("Has superado el record de tiradas anterior de " + rAlm + ".\nHas establecido el record en " + record + " tiradas, Enhorabuena.")
            localStorage.setItem("record", record);
        }
    }
}